//
//  Event.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/27/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

-(instancetype)initWithId:(NSString *)Id Nama:(NSString *)nama Tanggal:(NSString *)tanggal Deskripsi:(NSString *)deskripsi ImageURL:(NSString *)imageURL;

@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *nama;
@property (nonatomic, strong) NSString *tanggal;
@property (nonatomic, strong) NSString *deskripsi;
@property (nonatomic, strong) NSString *imageURL;

@end
