//
//  JSONLoader.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/27/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONLoader : NSObject

- (NSArray *)locationsFromJSONFile:(NSURL *)url;

@end
