//
//  EventTableViewController.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//


#import "EventTableViewController.h"
#import "MainViewController.h"
#import "Event.h"
#import "JSONLoader.h"

#define JSON_URL @"http://suitmedia-test.herokuapp.com/events"
#import "Event.h"
#import "EventCell.h"





@interface EventTableViewController (){
    NSArray *_events;
}



@end


@implementation EventTableViewController
@synthesize passEvent;

-(NSMutableArray *)objectHolderArray{
    if(!_objectHolderArray) _objectHolderArray = [[NSMutableArray alloc]init];
    return _objectHolderArray;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_validation.png"]];

    
    
    
    NSURL *blogURL = [NSURL URLWithString:JSON_URL];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:blogURL];
    
    NSError *error = nil;
    
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];

    
    for(NSDictionary *bpDictionary in dataDictionary){
        Event *event = [[Event alloc]initWithId:[bpDictionary objectForKey:@"Id"]
                                                 Nama:[bpDictionary objectForKey:@"nama"]
                                                 Tanggal:[bpDictionary objectForKey:@"tanggal"]
                                                 Deskripsi:[bpDictionary objectForKey:@"deskripsi"]
                                                 ImageURL:[bpDictionary objectForKey:@"imageURL"]];
        [self.objectHolderArray addObject:event];
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated{
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    passEvent=[_objectHolderArray objectAtIndex:indexPath.row];
//    [self performSegueWithIdentifier:@"segueEvent" sender:self];
    
    Event *event = [self.objectHolderArray objectAtIndex:indexPath.row];
    
    [self.delegate receiveMessage:event.nama];
    [self dismissViewControllerAnimated:YES completion:nil];

}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ([segue.identifier isEqualToString:@"segueEvent"]) {
//        MainViewController *mvc=[segue destinationViewController];
//        mvc.passedEvent = passEvent;
//    }
//    // [segue.destinationViewController setHidesBottomBarWhenPushed:YES];
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.objectHolderArray count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 
    
    
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventCell"];
    
    Event *event = [self.objectHolderArray objectAtIndex:indexPath.row];
    
    
    
    cell.lblNama.text = event.nama;
    cell.lblTanggal.text = event.tanggal;
    cell.lblDeskripsi.text = event.deskripsi;
    
    
    NSURL *imageURL = [NSURL URLWithString:event.imageURL];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    
    cell.imageView.image = image;
   
    

    
    return cell;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
