//
//  Event.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/27/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "Event.h"

@implementation Event


-(instancetype)initWithId:(NSString *)Id Nama:(NSString *)nama Tanggal:(NSString *)tanggal Deskripsi:(NSString *)deskripsi ImageURL:(NSString *)imageURL{
    
    self = [super init];
    if(self){
        self.Id=Id;
        self.nama = nama;
        self.tanggal=tanggal;
        self.deskripsi=deskripsi;
        self.imageURL=imageURL;
        
    }
    
    
    return self;
}
@end
