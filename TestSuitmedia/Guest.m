//
//  Guest.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/28/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "Guest.h"

@implementation Guest
-(instancetype)initWithId:(NSString *)Id Name:(NSString *)name Birthdate:(NSString *)birthdate{
    
    
    self = [super init];
    if(self){
        self.Id=Id;
        self.name = name;
        self.birthdate=birthdate;
     
        
    }
    
    return self;
}
@end
