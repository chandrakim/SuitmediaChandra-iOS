//
//  main.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/22/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
