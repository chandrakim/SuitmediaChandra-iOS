//
//  Guest.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/28/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Guest : NSObject

-(instancetype)initWithId:(NSString *)Id Name:(NSString *)name Birthdate:(NSString *)birthdate;

@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *birthdate;
@end
