//
//  ViewController.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/22/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "ViewController.h"
#import "MainViewController.h"
#import "IQKeyboardManager.h"

#import "FUIButton.h"


#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"


@interface ViewController ()

@end

@implementation ViewController
@synthesize txtUsernameHome, btnProfile;



- (IBAction)toggleUIButtonImage:(id)sender {
    UIButton *tempButton = (UIButton *)sender;
    if(tempButton.isSelected){
        [tempButton setSelected:NO];
    }else{
        [tempButton setSelected:YES];
    }
    
   
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    //[btnSelesai setImage:[UIImage imageNamed:@"btn_signup_normal.png"] forState:UIControlStateNormal];
    //[btnSelesai setImage:[UIImage imageNamed:@"btn_signup_selected.png"] forState:UIControlStateSelected];


    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_validation.png"]];
    
    [self changeColor];
    [self changeBackground];
     [self makeButtonShiny:self.btnSelesai withBackgroundColor:[UIColor whiteColor]];


    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void)changeColor
{
    [self.btnSelesai setBackgroundColor:[UIColor whiteColor]];
    // [[btnSelesai layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[self.btnSelesai layer] setCornerRadius:8.0f];
    [[self.btnSelesai layer] setBorderWidth:2.0f];
    

}

- (void)changeBackground
{
    UIImage *startBackgroundImage = [UIImage imageNamed:@"btn_signup_selected.png"];
    [self.btnSelesai setBackgroundImage:startBackgroundImage forState:UIControlStateNormal];
}

- (void)makeButtonShiny:(UIButton*)button withBackgroundColor:(UIColor*)backgroundColor
{
    // Get the button layer and give it rounded corners with a semi-transparant button
    CALayer *layer = button.layer;
    layer.cornerRadius = 8.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 3.0f;
    layer.borderColor = [UIColor colorWithWhite:0.6f alpha:0.4f].CGColor;
    
    // Create a shiny layer that goes on top of the button
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = button.layer.bounds;
    // Set the gradient colors
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         nil];
    // Set the relative positions of the gradient stops
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.6f],
                            [NSNumber numberWithFloat:1.0f],
                            nil];
    
    // Add the layer to the button
    [button.layer addSublayer:shineLayer];
    
    [button setBackgroundColor:backgroundColor];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissModalViewControllerAnimated:YES];
    
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"%@",image);
    [btnProfile setImage:image forState:UIControlStateNormal];
    
    
}


- (IBAction)addPP:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [imagePicker setDelegate:self];
    [self presentModalViewController:imagePicker animated:YES];

}



- (IBAction)btnSelesai:(id)sender {

    
    NSString *name = self.txtUsernameHome.text ;
    
   
        int len = [name length];
        NSMutableString *reverseName = [[NSMutableString alloc] initWithCapacity:len];
        
        for(int i=len-1;i>=0;i--)
        {
            [reverseName appendFormat:[NSString stringWithFormat:@"%c",[name characterAtIndex:i]]];
            
        }
        
        NSLog(@"%@",reverseName);
        
        NSString *result;
        
        if ([name isEqualToString:reverseName]) {
            result = [NSString stringWithFormat:@"%@ is Palindrome", name];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Result"
                                                            message:result
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            result = [NSString stringWithFormat:@"%@ not Palindrome", name];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Result"
                                                            message:@"not Palindrome"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }

    
    
    

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toMain"]) {
        MainViewController *mvc=[segue destinationViewController];
        mvc.username = self.txtUsernameHome.text;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
