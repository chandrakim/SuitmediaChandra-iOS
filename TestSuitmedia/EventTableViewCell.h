//
//  EventTableViewCell.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageEvent;

@property (weak, nonatomic) IBOutlet UILabel *namaEvent;
@property (weak, nonatomic) IBOutlet UILabel *tanggalEvent;
@property (weak, nonatomic) IBOutlet UILabel *deskripsiEvent;

@end
