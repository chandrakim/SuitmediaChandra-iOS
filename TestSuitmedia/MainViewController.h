//
//  MainViewController.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "ViewController.h"
#import "Event.h"

@protocol MainViewControllerDelegate<NSObject>
-(void)receiveMessage:(NSString *)message;

-(void)receiveMessage2:(NSString *)message2;
@end

@interface MainViewController : UIViewController<MainViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblUsernameMain;
@property (nonatomic, strong) NSString *username;



@property (weak, nonatomic) IBOutlet UIButton *btnEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnGuest;


@property (nonatomic,retain) Event *passedEvent;


@end
