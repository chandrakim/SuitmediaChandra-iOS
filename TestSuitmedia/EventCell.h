//
//  EventCell.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/27/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNama;
@property (weak, nonatomic) IBOutlet UILabel *lblTanggal;
@property (weak, nonatomic) IBOutlet UILabel *lblDeskripsi;
@property (weak, nonatomic) IBOutlet UIImageView *image;


@end
