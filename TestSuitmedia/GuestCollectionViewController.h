//
//  GuestCollectionViewController.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface GuestCollectionViewController : UICollectionViewController
@property (nonatomic,strong) NSMutableArray *objectHolderArray;


@property (weak, nonatomic) id<MainViewControllerDelegate> delegate;

@end
