//
//  GuestCellCollectionViewCell.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/28/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestCellCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNama;

@end
