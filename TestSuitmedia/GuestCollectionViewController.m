//
//  GuestCollectionViewController.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "GuestCollectionViewController.h"
#import "GuestCellCollectionViewCell.h"
#import "Guest.h"
#define JSON_URL @"http://dry-sierra-6832.herokuapp.com/api/people"

#include <math.h>

@interface GuestCollectionViewController (){
    NSArray *recipeImages;
}

@end

@implementation GuestCollectionViewController

-(NSMutableArray *)objectHolderArray{
    if(!_objectHolderArray) _objectHolderArray = [[NSMutableArray alloc]init];
    return _objectHolderArray;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_validation.png"]];

    //self.collectionView.backgroundColor = [UIColor whiteColor];
    
       recipeImages = [NSArray arrayWithObjects:@"foto-1.jpg", @"foto-2.jpg", @"foto-3.jpg", @"foto-4.jpg", @"foto-5.jpg", nil];
    
    NSURL *blogURL = [NSURL URLWithString:JSON_URL];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:blogURL];
    
    NSError *error = nil;
    
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    
    for(NSDictionary *bpDictionary in dataDictionary){
        Guest *guest = [[Guest alloc]initWithId:[bpDictionary objectForKey:@"Id"]
                                           Name:[bpDictionary objectForKey:@"name"]
                                        Birthdate:[bpDictionary objectForKey:@"birthdate"]];
        [self.objectHolderArray addObject:guest];
    }
  
}
-(void)viewDidAppear:(BOOL)animated{
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    
    Guest *guest = [self.objectHolderArray objectAtIndex:indexPath.row];
    
    NSArray* bd = [guest.birthdate componentsSeparatedByString: @"-"];
    NSString* c0 = [bd objectAtIndex: 0];
    NSString* c1 = [bd objectAtIndex: 1];
    NSString* c2 = [bd objectAtIndex: 2];
    
    
    int tanggal = [c2 intValue];
    int bulan = [c1 intValue];
    NSString *hasil;
    NSString *prime = @"Prime";
    
    if(tanggal % 2 == 0 && tanggal % 3 ==0){
        hasil = @"iOS";
    }else if(tanggal % 2 == 0 ){
        hasil = @"Blackberry";
    }else if(tanggal % 3 == 0 ){
        hasil = @"Android";
    }else{
        hasil = @"Feature Phone";
    }
    
    int length=sqrt(bulan);
    
    int sqrt = length+1;
    if(bulan == 1){
             prime = @"Not Prime";
    }else{
        for (int i = 2; i< sqrt; i++) {
            if(bulan % i ==0){
                prime = @"Not Prime";
            }
        }
    }
    
    
    
    NSString *a = [NSString stringWithFormat:@"%@\r%d is %@", hasil,bulan,prime];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Result"
                                                    message:a
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    [self.delegate receiveMessage2:guest.name];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return recipeImages.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    
    GuestCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:[recipeImages objectAtIndex:indexPath.row]];
    
    
    Guest *guest = [self.objectHolderArray objectAtIndex:indexPath.row];
    
    cell.lblNama.text = guest.name;
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame.png"]];
    
    return cell;
}

@end
