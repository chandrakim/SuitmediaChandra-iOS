//
//  MainViewController.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
#import "EventTableViewController.h"
#import "GuestCollectionViewController.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UIButton *responseButton;
@end

@implementation MainViewController
@synthesize lblUsernameMain, btnEvent, btnGuest, passedEvent;

@synthesize username;


- (void)viewDidLoad {
    [super viewDidLoad];

  
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_validation.png"]];
    
    lblUsernameMain.text = username;
    
    [self changeColor];
    [self changeBackground];
    [self makeButtonShiny:self.btnEvent withBackgroundColor:[UIColor whiteColor]];

    [self makeButtonShiny:self.btnGuest withBackgroundColor:[UIColor whiteColor]];

     
    
    // Do any additional setup after loading the view.
}

- (void)changeColor
{
    [self.btnEvent setBackgroundColor:[UIColor whiteColor]];

    [[self.btnEvent layer] setCornerRadius:8.0f];
    [[self.btnEvent layer] setBorderWidth:2.0f];
    
    [self.btnGuest setBackgroundColor:[UIColor whiteColor]];
        [[self.btnGuest layer] setCornerRadius:8.0f];
    [[self.btnGuest layer] setBorderWidth:2.0f];
    
    
}

- (void)changeBackground
{
    UIImage *startBackgroundImage = [UIImage imageNamed:@"btn_signup_selected.png"];
    [self.btnGuest setBackgroundImage:startBackgroundImage forState:UIControlStateNormal];
    
    UIImage *start2BackgroundImage = [UIImage imageNamed:@"btn_signup_selected.png"];
    [self.btnEvent setBackgroundImage:start2BackgroundImage forState:UIControlStateNormal];

}

- (void)makeButtonShiny:(UIButton*)button withBackgroundColor:(UIColor*)backgroundColor
{
    // Get the button layer and give it rounded corners with a semi-transparant button
    CALayer *layer = button.layer;
    layer.cornerRadius = 8.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 3.0f;
    layer.borderColor = [UIColor colorWithWhite:0.6f alpha:0.4f].CGColor;
    
    // Create a shiny layer that goes on top of the button
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = button.layer.bounds;
    // Set the gradient colors
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         nil];
    // Set the relative positions of the gradient stops
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:0.2f],
                            [NSNumber numberWithFloat:0.4f],
                            [NSNumber numberWithFloat:0.6f],
                            [NSNumber numberWithFloat:1.0f],
                            nil];
    
    // Add the layer to the button
    [button.layer addSublayer:shineLayer];
    
    [button setBackgroundColor:backgroundColor];
}


-(void)viewDidDisappear:(BOOL)animated{

    //[spinner stopAnimating];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    EventTableViewController *event = [segue destinationViewController];
    
    
    MainViewController *main = [segue sourceViewController];
    event.delegate = main;
    
   
    
    
}

-(void)prepareForSegue2:(UIStoryboardSegue *)segue sender:(id)sender {
    
    GuestCollectionViewController *guest = [segue destinationViewController];
    
    MainViewController *main = [segue sourceViewController];
   
    guest.delegate = main;
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)receiveMessage:(NSString *)message {
    [btnEvent setTitle:message forState:UIControlStateNormal];
    
    
    
    
}

- (void)receiveMessage2:(NSString *)message2 {
    
    
    [btnGuest setTitle:message2 forState:UIControlStateNormal];
    
}

- (IBAction)openSecondAction2:(id)sender {
    [self performSegueWithIdentifier:@"toGuest" sender:self];
//    
//    
//    spinner.center = CGPointMake(160, 240);
//    spinner.tag = 12;
//    
//    [self.view addSubview:spinner];
//    [spinner startAnimating];
}

- (IBAction)openSecondAction:(id)sender {
    [self performSegueWithIdentifier:@"toEvent" sender:self];
    
//    spinner.center = CGPointMake(160, 240);
//    spinner.tag = 12;
//    [self.view addSubview:spinner];
//    [spinner startAnimating];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
