//
//  EventCell.m
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/27/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) layoutSubviews{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(182, 0, 130, 130);
}

@end
