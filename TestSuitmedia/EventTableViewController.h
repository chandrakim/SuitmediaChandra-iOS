//
//  EventTableViewController.h
//  TestSuitmedia
//
//  Created by Chandra Welim on 3/23/16.
//  Copyright (c) 2016 ChandraWelim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "MainViewController.h"

@interface EventTableViewController : UITableViewController
@property (nonatomic,strong) NSMutableArray *objectHolderArray;
@property (nonatomic,retain) Event *passEvent;

@property (weak, nonatomic) id<MainViewControllerDelegate> delegate;

@end
